# Import Libraries
import os
import sys
import requests as r
import httputils
import cx_Oracle as cx
import mysql.connector
from ldap3 import Server, Connection, operation, SIMPLE, SYNC, ASYNC, SUBTREE, ALL
import logging
import time

# Logging
log_creation_time = time.strftime('%Y%m%d%H%M%S')
log_file = "C:\\temp\\python_log_" + log_creation_time +".log"
log_level = logging.DEBUG
logging.basicConfig(filename=log_file, filemode='w', format='%(created)f %(module)s %(message)s', datefmt='%Y%m%d%H%M%S', level=log_level)
log = logging.getLogger(__name__)

# DEBUG
DEBUG = True

# Variables
network_db_uri = "sirius.dbs.it.ubc.ca"
network_db_port = 1521
network_db_sid = "devl11b"
network_db_dsn = cx.makedsn(network_db_uri,network_db_port,network_db_sid)
network_db_con = None
network_db_conn_username = "mpal"
network_db_conn_password = "M1ch43lP"
network_db_query = "SELECT ROLE_NAME,VLAN_ID,VLAN_NAME,ROLE_VLAN_ID FROM NC_ROLE_VLAN_TBL"
network_db_rowcount = None

mysql_db_uri = "localhost"
mysql_db_port = 3306
mysql_db_dsn = "test"
mysql_db_conn = None
mysql_db_conn_username = "mpal"
mysql_db_conn_password = "S1mpl322!"
mysql_db_table = "UBC_CWLROLE"
mysql_db_add_query = None
mysql_db_delete_query = "DELETE FROM " + mysql_db_dsn + "." + mysql_db_table

eldap_host_uri = "ldaps://eldapprov.stg.id.ubc.ca"
#eldap_host_uri = "ldaps://eldapcons.stg.id.ubc.ca"
eldap_host_port = 636
eldap_server = None
eldap_server_conn = None
eldap_base_dn = "ou=VPN,ou=ANUTA,ou=Role,ou=Groups,ou=NMC,ou=SERVICES,dc=stg,dc=id,dc=ubc,dc=ca"
eldap_bind_account_username = "uid=nmc-svcanuta1,ou=Service Accounts,ou=NMC,ou=SERVICES,dc=stg,dc=id,dc=ubc,dc=ca"
eldap_bind_account_password = "tj4yeTNCGJ0dDt6X2Iah"
eldap_search_scope = SUBTREE
eldap_search_filter = "(objectClass=groupOfNames)"
#eldap_search_filter = "*"
eldap_add_attributes = {'objectClass': ['groupOfNames','nestedGroup','top'], 'cn': None, 'description': None}
eldap_add_dn = None
eldap_add_response = None
eldap_reader = None

ncx_rest_session = httputils.anutasession('restapi','S1mpl322!')

# Connect to Oracle Database
try:
    log.debug("Connecting to %s:%d/%s", network_db_uri, network_db_port, network_db_sid)
    network_db_con = cx.connect(network_db_conn_username, network_db_conn_password, network_db_dsn)
    network_db_con.ping()
    log.debug("Connected to %s:%d/%s/%s", network_db_uri, network_db_port, network_db_sid, netowrk_db_con.version)
    log.debug("Creating DB Cursor with %s", network_db_con.dsn)
    log.debug("Running query %s", network_db_query)
    network_db_cursor = network_db_con.cursor()
    # Execute Query
    network_db_cursor.execute(network_db_query)
    network_db_results = network_db_cursor.fetchall()
    network_db_rowcount = len(network_db_results)
    log.debug("Closing Oracle connection to %s:%d/%s", network_db_uri, network_db_port, network_db_sid)
    network_db_con.close()
except Exception, err:
    raise RuntimeError("Unexpected error during ORACLE operations")
    log.debug("Unexpected error during ORACLE connection")
    log.debug(err)
    log.debug("Exiting...Peace Out!")
    raise SystemExit


# Open connection to ELDAP Server
try:
    log.debug("Binding to LDAP Server %s:%d", eldap_host_uri, eldap_host_port)
    eldap_server = Server(eldap_host_uri, port=eldap_host_port, use_ssl=True, get_info=ALL)
    eldap_server_conn = Connection(eldap_server, auto_bind=True, client_strategy=SYNC, user=eldap_bind_account_username,password=eldap_bind_account_password, authentication=SIMPLE, check_names=True)
except Exception, err:
    raise RuntimeError("Unexpected error during LDAP Operations %s:%d", eldap_host_uri, eldap_host_port)
    log.debug("Unexpected error binding to LDAP Server %s:%d", eldap_host_uri, eldap_host_port)
    log.debug(err)
    log.debug("Exiting...Peace Out!")
    raise SystemExit


# Request groups from the ELDAP server
try:
    eldap_server_conn.search(search_base=eldap_base_dn,
             search_filter=eldap_search_filter,
             search_scope=SUBTREE,
             attributes=['cn'],
             #attributes=['*'],
             size_limit=1000)
    # Gather ELDAP results
    eldap_search_response = eldap_server_conn.response
    eldap_search_result = eldap_server_conn.result
    #for entry in eldap_search_response:
    #    print entry['raw_attributes']['cn'][0]
except:
    print "Unexpected error during ELDAP search:", sys.exc_info()[0]
    print "Peace Out!"
    raise SystemExit



# Compare Network DB results with ELDAP groups.
# Any DB entries that do no exist in ELDAP should be added to ELDAP
# Add groups to ELDAP
for row in network_db_results:
    match = 0

    for row2 in eldap_search_response:
        if row[0] == row2['raw_attributes']['cn'][0]:
            match = 1
            break
    if match == 0:
        if DEBUG:
            print "Adding %s group to %s:%d/%s" %  (row[0], eldap_host_uri, eldap_host_port, eldap_base_dn)
        eldap_add_attributes['cn'] = str(row[0])
        eldap_add_attributes['description'] = "ROLE_VLAN_ID=" + str(row[3])
        eldap_add_dn = "cn=" + str(row[0]) + "," + eldap_base_dn
        #eldap_add_request = operation.add.add_operation(eldap_add_dn, eldap_add_attributes)
        eldap_server_conn.add(eldap_add_dn,None,eldap_add_attributes)
        eldap_add_response = eldap_server_conn.response
     
# Print out ELDAP results
if DEBUG:
    if eldap_add_response is not None:
        print eldap_add_response

# Connect to MySQL Database
if DEBUG:
    print "Connecting to %s:%d/%s" %  (mysql_db_uri, mysql_db_port, mysql_db_dsn)
try:
    mysql_db_conn = mysql.connector.connect(user=mysql_db_conn_username,password=mysql_db_conn_password,host=mysql_db_uri,database=mysql_db_dsn)
    mysql_db_cursor = mysql_db_conn.cursor()
except:
    print "Unexpected error during MySQL connection:", sys.exc_info()[0]
    print "Peace Out!"
    raise SystemExit

# Delete old CWL Role records
if DEBUG:
    print "Deleting CWL Roles from %s:%d/%s.%s" %  (mysql_db_uri, mysql_db_port, mysql_db_dsn, mysql_db_table)
    print mysql_db_delete_query
mysql_db_cursor.execute(mysql_db_delete_query)

# Add CWL Roles to Radiator
if DEBUG:
    print "Adding %d groups to %s:%d/%s" %  (network_db_rowcount, mysql_db_uri, mysql_db_port, mysql_db_dsn)
for row in network_db_results:
    mysql_db_add_query = "INSERT INTO " + mysql_db_dsn + "." + mysql_db_table + " (CWL_ROLE, VLAN_ID, VLAN_NAME) VALUES ('" + str(row[0]) + "', '" + str(row[1]) + "', '" + str(row[2]) + "')"
    if DEBUG:
        print mysql_db_add_query
    mysql_db_cursor.execute(mysql_db_add_query)


# Close connection to Oracle Database
if DEBUG:
    print "Closing connection to %s:%d/%s" %  (network_db_uri, network_db_port, network_db_sid)
try:
    network_db_con.close()
except:
    print "Unexpected error during closing ORACLE connection:", sys.exc_info()[0]
    print "Peace Out!"
    raise SystemExit

# Disconnect from MySQL Database
if DEBUG:
    print "Closing connection to %s:%d/%s" %  (mysql_db_uri, mysql_db_port, mysql_db_dsn)
try:
    mysql_db_conn.commit()
    mysql_db_cursor.close()
    mysql_db_conn.close()
except:
    print "Unexpected error during closing MySQL connection:", sys.exc_info()[0]
    print "Peace Out!"
    raise SystemExit


# Close ELDAP connection
if DEBUG:
    print "Unbinding from LDAP Server %s:%d" %  (eldap_host_uri, eldap_host_port)
try:
    eldap_server_conn.unbind()
except:
    print "Unexpected error during ELDAP unbind:", sys.exc_info()[0]
    print "Peace Out!"
    raise SystemExit

SystemExit(0)